/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.examen.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.is2t1.examen2t1.viewa.MainFrame;
import com.is2t1.examen2t1.viewa.EmployeeFrame;

/**
 *
 * @author Sistemas-16
 */
public class MainController implements ActionListener{
    MainFrame mainFrame;

    public MainController(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        switch(event.getActionCommand()){
            case "Empleado":
                showEmployeeFrame();
                break;
            case "salir":
                System.exit(0);
                break;
        }
    }
    private void showEmployeeFrame(){
        EmployeeFrame pf = new EmployeeFrame();
        mainFrame.showChild(pf, false);
    }
    
    
}
