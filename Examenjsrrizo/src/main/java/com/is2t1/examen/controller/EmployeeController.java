/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.examen.controller;

/**
 *
 * @author Sistemas-16
 */
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import com.is2t1.examen.models.Employee;
import com.is2t1.examen2t1.viewa.EmployeeFrame;
/**
 *
 * @author JUNIOR RIZO
 */
public class EmployeeController implements ActionListener, FocusListener {

    EmployeeFrame personFrame;
    JComboBox dComboBox;
    JComboBox mComboBox;
    JFileChooser d;
    Employee employee;
    String[] oficio = new String[]{
        "Carpintero","Albañil", "Electricista", "Soldador", "Maestro de obra"};
    
    DefaultComboBoxModel departmentComboBoxModel = new DefaultComboBoxModel<>(oficio);

    public EmployeeController(EmployeeFrame f) {
        super();
        personFrame = f;
        d = new JFileChooser();
        employee = new Employee();
    }

    public void setemployee(Employee b) {
        employee = b;
    }

    public void setPerson(String filePath) {
        File f = new File(filePath);
        readPerson(f);
    }

    public Employee getPerson() {
        return employee;
    }

    public void setDepartmentComboBox(JComboBox jcombo) {
        dComboBox = jcombo;
        dComboBox.setModel(departmentComboBoxModel);
    }

    public void setMunicipalityComboBox(JComboBox jcombo) {
        mComboBox = jcombo;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().getClass().getName().equals(JComboBox.class.getName()))
        {
            JComboBox cbo = (JComboBox)e.getSource();
            switch(cbo.getName()){
                case "oficio":
                    
                    
            }
        } else {
            switch (e.getActionCommand()) {
                case "save":
                    d.showSaveDialog(personFrame);
                    employee = personFrame.getEmployeeData();
                    writePerson(d.getSelectedFile());
                    break;
                case "select":
                    d.showOpenDialog(personFrame);
                    employee = readPerson(d.getSelectedFile());
                    personFrame.setEmployeeData(employee);
                    break;
                case "clear":
                    personFrame.clear();
                    break;

            }
        }
    }

    @Override
    public void focusGained(FocusEvent arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent arg0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void writePerson(File file) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(getPerson());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Employee readPerson(File file) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            return (Employee) ois.readObject();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(personFrame, e.getMessage(), personFrame.getTitle(), JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(EmployeeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private Employee[] readPersonList(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        Employee[] employeers;
        try (FileInputStream in = new FileInputStream(file);
                ObjectInputStream s = new ObjectInputStream(in)) {
            employeers = (Employee[]) s.readObject();
        }
        return employeers;
    }

}
