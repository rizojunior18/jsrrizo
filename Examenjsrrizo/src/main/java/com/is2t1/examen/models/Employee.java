/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.examen.models;

import java.util.Date;

/**
 *
 * @author Sistemas-16
 */
public class Employee {
    private String id;
    private String Name;
    private String SecondName;
    private String lastName;
    private String SecondlasteName;
    private Date birthDate;
    private Date EmployeDate;
    private int age;
    private int ExperiensAge;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String SecondName) {
        this.SecondName = SecondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondlasteName() {
        return SecondlasteName;
    }

    public void setSecondlasteName(String SecondlasteName) {
        this.SecondlasteName = SecondlasteName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getEmployeDate() {
        return EmployeDate;
    }

    public void setEmployeDate(Date EmployeDate) {
        this.EmployeDate = EmployeDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getExperiensAge() {
        return ExperiensAge;
    }

    public void setExperiensAge(int ExperiensAge) {
        this.ExperiensAge = ExperiensAge;
    }
    
    
    
}
